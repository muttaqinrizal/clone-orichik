import React, { Component } from 'react';
import { Container, Typography } from '@material-ui/core';
import BottomNavigations from '../../component/bottom-navigation';
import Card from "../../component/card";
import Makanan1 from "../../asset/makanan1.png";
import Stok from "../../component/stok";
class AnalitycScreen extends Component {
  state = {}
  render() {
    return (<React.Fragment>
      <Container maxWidth="xs" style={{ padding: 0 }}>
        <div style={{ backgroundColor: "#FF6B6B", height: 24 }}></div>
        <div>
          <Typography
            style={{
              fontFamily: "Avenir",
              fontSize: 30,
              margin: "25px 0px 21px 0px",
              textAlign: "center"
            }}
          >
            Daftar Stok Hari Ini
          </Typography>
        </div>
        <div style={{ margin: "30px 20px" }}>
          <div
            style={{
              display: "flex",
              justifyContent: "space-between",
              fontFamily: "Avenir",
              fontSize: 18
            }}
          >
            <Typography>Nama Produk</Typography>
            <Typography>Stok</Typography>
          </div>
          <hr style={{ border: "0.5px solid rgba(41, 47, 54, 0.15)" }} />
          <div>
            <Stok
              gambar={Makanan1}
              paket="Paket 1"
              harga="16.000"
              detail="Nasi + Dada + Es teh"
              jumlah="10"
            />
            <Stok
              gambar={Makanan1}
              paket="Paket 2"
              harga="10.000"
              detail="Nasi + Sayap + Es teh"
              jumlah="15"
            />
            <Stok
              gambar={Makanan1}
              paket="Paket 2"
              harga="10.000"
              detail="Nasi + Sayap + Es teh"
              jumlah="15"
            /><Stok
              gambar={Makanan1}
              paket="Paket 2"
              harga="10.000"
              detail="Nasi + Sayap + Es teh"
              jumlah="15"
            /><Stok
              gambar={Makanan1}
              paket="Paket 2"
              harga="10.000"
              detail="Nasi + Sayap + Es teh"
              jumlah="15"
            /><Stok
              gambar={Makanan1}
              paket="Paket 2"
              harga="10.000"
              detail="Nasi + Sayap + Es teh"
              jumlah="15"
            /><Stok
              gambar={Makanan1}
              paket="Paket 2"
              harga="10.000"
              detail="Nasi + Sayap + Es teh"
              jumlah="15"
            />
          </div>
        </div>
        <BottomNavigations />
      </Container>
    </React.Fragment>);
  }
}

export default AnalitycScreen;