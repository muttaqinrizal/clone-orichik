import React, { Component } from "react";
import {
  Container,
  AppBar,
  Typography,
  IconButton,
  Dialog,
  DialogTitle,
  DialogContent,
  DialogContentText,
  TextField,
  DialogActions,
  Button
} from "@material-ui/core";
import MenuIcon from "@material-ui/icons/Menu";
import ArrowBackIcon from "@material-ui/icons/ArrowBack";
import CardOrder from "../../component/card-order";

class RingkasanScreen extends Component {
  state = {
    open: false,
    data: [
      {
        jumlah: 6,
        paket: "Paket 1",
        harga: 16000,
        diskon: 30
      },
      {
        jumlah: 9,
        paket: "Paket 2",
        harga: 19000,
        diskon: 10
      },
      {
        jumlah: 2,
        paket: "Paket 3",
        harga: 10000,
        diskon: 20
      }
    ]
  };

  handleClickOpen = () => {
    this.setState({ open: true });
  };

  handleClose = () => {
    this.setState({ open: false });
  };

  render() {
    return (
      <React.Fragment>
        <Container maxWidth="xs" style={{ fontFamily: "Avenir", padding: 0 }}>
          <div style={{ backgroundColor: "#F55F44", height: 24 }}></div>
          <AppBar
            position="static"
            style={{
              background: "#FF6B6B",
              display: "flex",
              flexDirection: "row",
              alignItems: "center"
            }}
          >
            <IconButton>
              <ArrowBackIcon style={{ color: "white" }} />
            </IconButton>
            <Typography>Ringkasan Order</Typography>
          </AppBar>
          <Dialog
            open={this.state.open}
            onClose={this.handleClose}
            aria-labelledby="form-dialog-title"
          >
            <DialogTitle id="form-dialog-title">Subscribe</DialogTitle>
            <DialogContent>
              <DialogContentText>test</DialogContentText>
            </DialogContent>
            <DialogActions>
              <Button onClick={this.handleClose} color="primary">
                Cancel
              </Button>
              <Button onClick={this.handleClose} color="primary">
                Subscribe
              </Button>
            </DialogActions>
          </Dialog>
          {this.state.data.map(res => {
            return (
              <div onClick={this.handleClickOpen}>
                <CardOrder
                  jumlah={res.jumlah}
                  paket={res.paket}
                  harga={res.harga}
                  diskon={res.diskon}
                />
              </div>
            );
          })}

          <div
            style={{
              display: "flex",
              justifyContent: "space-between",
              alignItems: "center",
              position: "fixed",
              width: "100%",
              height: 50,
              backgroundColor: "#4ECDC4",
              bottom: 0,
              color: "white"
            }}
          >
            <Typography style={{ marginLeft: "5%" }}>Total</Typography>
            <Typography style={{ marginRight: "5%" }}>Rp 300.000</Typography>
          </div>
        </Container>
      </React.Fragment>
    );
  }
}

export default RingkasanScreen;
