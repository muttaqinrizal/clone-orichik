import React, { Component } from "react";
import {
  Container,
  Typography,
  BottomNavigationAction,
  BottomNavigation
} from "@material-ui/core";
import Header from "../../asset/pos.png";
import Makanan1 from "../../asset/makanan1.png";
import Plus from "../../asset/plus.png";
import Card from "../../component/card";
import Minus from "../../asset/minus.png";
import BottomNavigations from "../../component/bottom-navigation";
import CardSale from "../../component/card-sale";

class PosScreen extends Component {
  state = {
    makanan: [
      {
        gambar: Makanan1,
        paket: "Paket 1",
        harga: "19.000"
      },
      {
        gambar: Makanan1,
        paket: "Paket 2",
        harga: "14.000"
      },
      {
        gambar: Makanan1,
        paket: "Paket 3",
        harga: "11.000"
      }
    ]
  };
  render() {
    return (
      <React.Fragment>
        <Container maxWidth="xs" style={{ padding: 0 }}>
          {/* <div style={{ backgroundColor: "#F55F44", height: 24 }}></div> */}
          <div
            style={{
              backgroundImage: "url(" + Header + ")",
              backgroundSize: "cover",
              height: 150
            }}
          >
            <Typography
              style={{
                paddingTop: 25,
                color: "white",
                textAlign: "center"
              }}
            >
              Pembelian Barang
            </Typography>
          </div>
          <div
            style={{
              display: "flex",
              justifyContent: "center",
              marginTop: -80
            }}
          >
            <div
              style={{
                background: "white",
                height: 100,
                width: "80%",
                borderRadius: 5,
                boxShadow: "0px 5px 30px rgba(41, 47, 54, 0.1)",
                display: "flex",
                flexDirection: "column",
                alignItems: "center",
                justifyContent: "center",
                color: "#FF6B6B",
                fontFamily: "Avenir"
              }}
            >
              <Typography>Biaya</Typography>
              <Typography style={{ fontSize: 30 }}>Rp 0</Typography>
            </div>
          </div>
          <div style={{ margin: "30px 20px" }}>
            <Typography>Barang</Typography>
            <hr style={{ border: "0.5px solid rgba(41, 47, 54, 0.15)" }} />
            {this.state.makanan.map(res => {
              return (
                <Card gambar={res.gambar} paket={res.paket} harga={res.harga} />
              );
            })}
            {/* <Card gambar={Makanan1} paket="Paket 1" harga="16.000" />
            <Card gambar={Makanan1} paket="Paket 2" harga="20.000" />
            <Card gambar={Makanan1} paket="Paket 2" harga="20.000" />
            <Card gambar={Makanan1} paket="Paket 2" harga="20.000" />
            <Card gambar={Makanan1} paket="Paket 2" harga="20.000" />
            <Card gambar={Makanan1} paket="Paket 2" harga="20.000" />
            <Card gambar={Makanan1} paket="Paket 2" harga="20.000" /> */}
            <CardSale />
          </div>
          <BottomNavigations />
        </Container>
      </React.Fragment>
    );
  }
}

export default PosScreen;
