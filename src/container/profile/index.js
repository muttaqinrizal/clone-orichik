import React, { Component } from "react";
import { Container, Typography } from "@material-ui/core";
import Header from "../../asset/pos.png";
import Outlet from "../../asset/icons/outlet.png";
import Phone from "../../asset/icons/phone.png";
import Location from "../../asset/icons/location.png";
import CardProfile from "../../component/card-profile";
import BottomNavigations from "../../component/bottom-navigation";
import Poto from "../../asset/poto.png";

class ProfileScreen extends Component {
  state = {};
  render() {
    return (
      <React.Fragment>
        <Container maxWidth="xs" style={{ padding: 0 }}>
          <div style={{ backgroundColor: "#F55F44", height: 24 }}></div>
          <div
            style={{
              backgroundImage: "url(" + Header + ")",
              backgroundSize: "cover",
              height: 120
            }}
          ></div>
          <div
            style={{
              display: "flex",
              justifyContent: "center",
              marginTop: -80
            }}
          >
            <div
              style={{
                background: "white",
                height: 150,
                width: "80%",
                borderRadius: 5,
                boxShadow: "0px 5px 30px rgba(41, 47, 54, 0.1)",
                display: "flex",
                flexDirection: "column",
                alignItems: "center",
                justifyContent: "center",
                fontFamily: "Avenir"
              }}
            >
              <img src={Poto} />
              <Typography>Rizal Muttaqin</Typography>
              <Typography
                style={{ fontSize: 12, color: "rgba(41, 47, 54, 0.5)" }}
              >
                Sedang Buka
              </Typography>
            </div>
          </div>
          <div>
            <CardProfile
              title="Outlet"
              content="Cepoko | Semarang"
              icons={Outlet}
            />
            <CardProfile
              title="Alamat Outlet"
              content="Jl. Raya Manyaran-Gunungpati, Cepoko"
              icons={Phone}
            />
            <CardProfile
              title="Nomor HP"
              content="0812 - 3456 - 7890"
              icons={Location}
            />
          </div>
          <BottomNavigations />
        </Container>
      </React.Fragment>
    );
  }
}

export default ProfileScreen;
