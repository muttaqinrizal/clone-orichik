import React, { Component } from "react";
import { Container, Typography, Button } from "@material-ui/core";
import CameraAlt from "@material-ui/icons/CameraAlt";

class HasilFotoScreen extends Component {
  state = {};
  render() {
    return (
      <React.Fragment>
        <Container maxWidth="xs">
          <div style={{ backgroundColor: "#FF6B6B", height: 24 }}></div>
          <div
            style={{
              display: "flex",
              flexDirection: "column",
              alignItems: "center",
              justifyContent: "center"
            }}
          >
            <div style={{ marginTop: 132, width: 136 }}>
              <img />
            </div>
            <div
              style={{
                textAlign: "center",
                fontFamily: "Avenir",
                marginTop: 24
              }}
            >
              <Typography>Ayo selfie ! </Typography>
              <Typography>
                Silahkan selfie dengan gerobak daganganmu sebagai bukti
                gerobakmu buka.
              </Typography>
            </div>
            <Button
              variant="contained"
              style={{
                background: "#4ECDC4",
                width: 230,
                color: "white",
                marginTop: 20
              }}
            >
              <CameraAlt />
            </Button>
          </div>
        </Container>
      </React.Fragment>
    );
  }
}

export default HasilFotoScreen;
