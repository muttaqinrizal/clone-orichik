import React, { Component } from "react";
import Logo from "../../asset/ORICHICK.png";
import Backdrop from "../../asset/Shadow.png";
import { Typography, Container, Button } from "@material-ui/core";
import BottomNavigations from "../../component/bottom-navigation";
import Pos from "../../asset/icons/pos.png";
import Analityc from "../../asset/icons/analityc.png";
import Profile from "../../asset/icons/profile.png";
import { BottomNavigation, BottomNavigationAction } from "@material-ui/core";
import { Link } from "react-router-dom";

class Beranda extends Component {
  state = {};
  render() {
    return (
      <React.Fragment>
        <Container maxWidth="xs" style={{ padding: 0 }}>
          <div style={{ backgroundColor: "#F55F44", height: 24 }}></div>
          <div
            style={{
              backgroundColor: "#FF6B6B",
              padding: "17px 0px",
              display: "flex",
              justifyContent: "center",
              alignItems: "center"
            }}
          >
            <img
              src={Logo}
              alt="logo"
              style={{
                width: 115,
                height: 21
                // display: "block",
                // margin: "0 auto"
              }}
            />
          </div>
          <div
            style={{
              display: "flex",
              flexDirection: "column",
              alignItems: "center",
              fontFamily: "Avenir",
              backgroundImage: "url(" + Backdrop + ")",
              height: 180,
              backgroundSize: "cover",
              color: "white"
            }}
          >
            <Typography variant="subtitle2" style={{ marginTop: 46 }}>
              OUTLET
            </Typography>
            <Typography variant="h5">CEPOKO</Typography>
            <Typography variant="caption">abcvde</Typography>
          </div>

          <div
            style={{
              display: "flex",
              flexDirection: "column",
              alignItems: "center",
              fontFamily: "Avenir",
              color: "#292F36",
              marginTop: 115
            }}
          >
            <Typography>Hai, Abdullah, sudah buka belum gerobaknya?</Typography>
            <Typography> Yuk mulai jualan sekarang</Typography>
            <Button
              style={{
                width: 230,
                fontFamily: "Avenir",
                alignContent: "center",
                textTransform: "capitalize",
                background: "#4ECDC4",
                color: "white",
                marginTop: 30,
                fontSize: 20
              }}
              component={Link}
              to="/foto"
              variant="contained"
            >
              Mulai Jualan
            </Button>
          </div>
          {/* <BottomNavigations /> */}
        </Container>
      </React.Fragment>
    );
  }
}

const Styles = {};

export default Beranda;
