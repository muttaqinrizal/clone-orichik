import React, { Component } from "react";
import { Container, CssBaseline, TextField, Button } from "@material-ui/core";
import Logo from "../../asset/ORICHICK.png";
import { Link } from "react-router-dom";

class Login extends Component {
  state = {};
  render() {
    return (
      <React.Fragment>
        <Container maxWidth="xs" style={styles.containerStyle}>
          <div style={styles.displayStyle}>
            <img src={Logo} alt="Logo" />
            <form>
              <TextField
                label="username"
                fullWidth
                margin="normal"
                variant="outlined"
              />
              <TextField
                label="password"
                type="password"
                fullWidth
                margin="normal"
                variant="outlined"
              />
            </form>

            <Button
              style={styles.btnStyle}
              variant="contained"
              component={Link}
              to="/beranda"
            >
              Masuk
            </Button>
          </div>
        </Container>
      </React.Fragment>
    );
  }
}

const styles = {
  containerStyle: {
    display: "flex",
    justifyContent: "center",
    height: "100vh"
  },
  displayStyle: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "column"
  },
  btnStyle: {
    width: 230,
    fontFamily: "Avenir",
    textTransform: "capitalize",
    background: "#FF6B6B",
    color: "white",
    fontSize: 20
  }
};

export default Login;
