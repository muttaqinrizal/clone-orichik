import React, { Component } from "react";
import { Container, CssBaseline, Typography, Button } from "@material-ui/core";
import Congrats from "../../asset/berhasil.png";

class Berhasil extends Component {
  state = {};
  render() {
    return (
      <React.Fragment>
        <Container maxWidth="xs">
          <CssBaseline />
          <div style={{ backgroundColor: "#FF6B6B", height: 24 }}></div>
          <div
            style={{
              display: "flex",
              flexDirection: "column",
              alignItems: "center"
            }}
          >
            <div style={{ marginTop: 132, width: 200 }}>
              <img src={Congrats} />
            </div>
            <div
              style={{
                textAlign: "center",
                fontFamily: "Avenir",
                marginTop: 24
              }}
            >
              <Typography>Berhasil ! </Typography>
              <Typography>
                Sekarang kamu sudah bisa mulai jualan. Semoga laris!
              </Typography>
            </div>
            <Button
              variant="contained"
              style={{
                background: "#4ECDC4",
                width: 230,
                color: "white",
                marginTop: 20
              }}
            >
              OK
            </Button>
          </div>
        </Container>
      </React.Fragment>
    );
  }
}

export default Berhasil;
