import React, { Component } from "react";
import { Switch, Route } from "react-router-dom";
import Login from "./container/login";
import Beranda from "./container/beranda";
import Foto from "./container/foto";
import Splash from "./container/splash";
import Berhasil from "./container/berhasil";
import PosScreen from "./container/pos";
import StokScreen from "./container/daftar-stok";
import RingkasanScreen from "./container/ringkasan";
import HasilFotoScreen from "./container/hasil-foto";
import ProfileScreen from "./container/profile";
import AnalitycScreen from "./container/analityc";
import Apps from "./container/app";

class App extends Component {
  state = {};
  render() {
    return (
      <Switch>
        <Route path="/login" component={Login} />
        <Route path="/beranda" component={Beranda} />
        <Route path="/foto" component={Foto} />
        <Route exact path="/" component={Splash} />
        <Route path="/berhasil" component={Berhasil} />
        <Route path="/daftar-stok" component={StokScreen} />
        <Route path="/pos" component={PosScreen} />
        <Route path="/ringkasan-order" component={RingkasanScreen} />
        <Route path="/hasil-foto" component={HasilFotoScreen} />
        <Route path="/profile" component={ProfileScreen} />
        <Route path="/analityc" component={AnalitycScreen} />
        <Route path="/apps" component={Apps} />
      </Switch>
    );
  }
}

export default App;
