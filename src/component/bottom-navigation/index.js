import React from "react";
import Pos from "../../asset/icons/pos.png";
import Analityc from "../../asset/icons/analityc.png";
import Profile from "../../asset/icons/profile.png";
import { BottomNavigation, BottomNavigationAction } from "@material-ui/core";
import { Link } from "react-router-dom";
function BottomNavigations() {
  return (
    <div
      style={{
        position: "fixed",
        left: 0,
        bottom: 0,
        width: "100%"
      }}
    >
      <BottomNavigation showLabels>
        <BottomNavigationAction
          label="POS"
          icon={<img src={Pos} style={{ marginBottom: 5 }} />}
          component={Link}
          to="/pos"
        ></BottomNavigationAction>
        <BottomNavigationAction
          label="ANALITYC"
          icon={<img src={Analityc} style={{ marginBottom: 5 }} />}
          component={Link}
          to="/analityc"
        ></BottomNavigationAction>
        <BottomNavigationAction
          label="PROFILE"
          icon={<img src={Profile} style={{ marginBottom: 5 }} />}
          component={Link}
          to="/profile"
        ></BottomNavigationAction>
      </BottomNavigation>
    </div>
  );
}

export default BottomNavigations;
