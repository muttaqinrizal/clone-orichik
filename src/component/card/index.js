import React from "react";
import { Typography } from "@material-ui/core";
import Plus from "../../asset/plus.png";

function Card(props) {
  return (
    <React.Fragment>
      <div
        style={{
          display: "flex",
          justifyContent: "space-between",
          margin: "15px 0px"
        }}
      >
        <div style={{ display: "flex", flexDirection: "row" }}>
          <img src={props.gambar} />
          <div
            style={{
              marginLeft: 15,
              fontFamily: "Avenir",
              fontSize: "16px",
              lineHeight: "22px"
            }}
          >
            <Typography style={{}}>{props.paket}</Typography>
            <Typography style={{ color: "#4ECDC4" }}>
              Rp {props.harga}
            </Typography>
          </div>
        </div>
        <div
          style={{
            display: "flex",
            justifyContent: "center",
            alignItems: "center"
          }}
        >
          <img
            src={Plus}
            style={{
              width: 30,
              height: 30
            }}
          />
        </div>
      </div>
    </React.Fragment>
  );
}

export default Card;
