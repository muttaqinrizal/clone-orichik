import React from 'react'
import { Container, Typography } from '@material-ui/core';


function CardProfile(props) {
  return (
    <div style={{
      display: "flex",
      flexDirection: 'column',
      margin: '5% 10%'
    }}>
      <div>
        <Typography>{props.title}</Typography>
      </div>
      <div >
        <div style={{
          background: "white",
          height: 50,
          width: '100%',
          borderRadius: 5,
          boxShadow: "0px 5px 30px rgba(41, 47, 54, 0.1)",
          display: "flex",
          alignItems: "center",
          fontFamily: "Avenir"
        }}>
          <img src={props.icons} style={{ width: 20, height: 18, margin: '5%' }} />
          <Typography style={{ fontSize: 14 }}>{props.content}</Typography>
        </div>
      </div>
    </div>
  )
}

export default CardProfile