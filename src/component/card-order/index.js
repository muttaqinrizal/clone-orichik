import React from "react";
import { Typography } from "@material-ui/core";

function CardOrder(props) {
  const diskon = (diskon, harga) => {
    const hasil = (diskon / 100) * harga;
    return hasil;
  };

  const total = (jumlah, harga) => {
    const hasil = jumlah * harga;
    return hasil;
  };
  return (
    <React.Fragment>
      <div style={{ margin: "10px 20px" }}>
        <div
          style={{
            display: "flex",
            alignItems: "center",
            justifyContent: "space-between"
          }}
        >
          <div style={{ display: "flex", alignItems: "center" }}>
            <div
              style={{
                backgroundColor: "#4ECDC4",
                borderRadius: 6,
                width: 56,
                height: 56,
                display: "flex",
                alignItems: "center",
                justifyContent: "center"
              }}
            >
              <Typography>{props.jumlah}</Typography>
            </div>
            <div style={{ marginLeft: 15 }}>
              <Typography>{props.paket}</Typography>
              <Typography
                style={{
                  fontSize: 10,
                  color: "rgba(41, 47, 54, 0.5)"
                }}
              >
                Diskon {props.diskon}%
              </Typography>
            </div>
          </div>
          <div>
            <Typography>Rp {total(props.jumlah, props.harga)}</Typography>
            <Typography
              style={{
                fontSize: 10,
                color: "rgba(41, 47, 54, 0.5)",
                display: "flex",
                justifyContent: "flex-end"
              }}
            >
              - {diskon(props.diskon, total(props.jumlah, props.harga))}
            </Typography>
          </div>
        </div>
        <hr style={{ border: "0.5px solid rgba(41, 47, 54, 0.15)" }} />
      </div>
    </React.Fragment>
  );
}

export default CardOrder;
