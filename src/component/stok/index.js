import React from "react";
import { Typography } from "@material-ui/core";
import Plus from "../../asset/plus.png";

function Stok(props) {
  return (
    <React.Fragment>
      <div
        style={{
          display: "flex",
          justifyContent: "space-between",
          margin: "15px 0px"
        }}
      >
        <div style={{ display: "flex", flexDirection: "row" }}>
          <img src={props.gambar} />
          <div
            style={{
              marginLeft: 15,
              fontFamily: "Avenir"
            }}
          >
            <Typography style={{ fontSize: 16 }}>{props.paket}</Typography>
            <Typography
              style={{ color: "rgba(41, 47, 54, 0.5)", fontSize: 12 }}
            >
              {props.detail}
            </Typography>
            <Typography style={{ color: "#4ECDC4", fontSize: 12 }}>
              Rp {props.harga}
            </Typography>
          </div>
        </div>
        <div
          style={{
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
            color: "#FF6B6B"
          }}
        >
          <Typography>{props.jumlah}</Typography>
        </div>
      </div>
    </React.Fragment>
  );
}

export default Stok;
