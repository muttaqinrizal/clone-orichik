import React from "react";
import Plus from "../../asset/plus.png";
import Minus from "../../asset/minus.png";
import { Typography } from "@material-ui/core";
function CardSale() {
  return (
    <React.Fragment>
      <div
        style={{
          display: "flex",
          justifyContent: "space-between",
          margin: "15px 0px",
          background: "#FFE66D",
          borderRadius: 6
        }}
      >
        <div
          style={{
            display: "flex",
            flexDirection: "row"
          }}
        >
          <div
            style={{
              backgroundColor: "#4ECDC4",
              borderRadius: 6,
              width: 56,
              height: 56,
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
              color: "white"
            }}
          >
            <Typography>10</Typography>
          </div>
          <div
            style={{
              marginLeft: 15,
              fontFamily: "Avenir",
              fontSize: "16px",
              lineHeight: "22px"
            }}
          >
            <Typography style={{}}>Paket 5</Typography>
            <Typography style={{ color: "#4ECDC4" }}>Rp 80.000</Typography>
          </div>
        </div>
        <div
          style={{
            display: "flex",
            justifyContent: "center",
            alignItems: "center"
          }}
        >
          <img
            src={Minus}
            style={{
              width: 30,
              height: 30
            }}
          />
          <img
            src={Plus}
            style={{
              width: 30,
              height: 30
            }}
          />
        </div>
      </div>
    </React.Fragment>
  );
}

export default CardSale;
